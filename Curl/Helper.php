<?php
/**
 * Copyright © 2016 Dombi István All rights reserved.
 * See COPYING.txt for license details.
 */
/**
 * PHP version 5.6
 *
 * @category  Aion
 * @author    Dombi István <dombi.istvan@webdice.hu>
 * @copyright 2015 AionNext Kft. (http://www.aion.hu)
 * @link      http://www.webdice.hu
 */
namespace Webdice\Utilities\Curl;

class Helper
{
    const ERROR_NO_URL = 'There is no URL specified, or not appropriate';
    const ERROR_CURL_OPTIONS_TYPE = 'The CURL options has to be array';
    const ERROR_CURL_EMPTY_PARAM_OR_VALUE = 'The CURL option or its value is empty';
    const ERROR_CURL_REQUEST_FAILED = 'The request has failed: %S';
    const ERROR_CURL_INVALID_OPTION = 'The parameter passed `%s` is not valid.';
    const ERROR_CURL_OPTION_EMPTY = 'The option key is empty.';
    const ERROR_CURL_OPTION_MISSING = 'The following parameter is missing: %s';
}