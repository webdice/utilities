<?php
/**
 * Copyright © 2016 AionNext Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
/**
 * PHP version 5
 *
 * @author    Dombi István <dombi.istvan@webdice.hu>
 * @copyright 2016 Dombi István (http://www.webdice.hu)
 * @link      http://www.webdice.com
 */
namespace Webdice\Utilities\Xml;

class Parser
{
    /** @var null|string */
    private $absPath = null;
    /** @var null|\SimpleXMLElement */
    private $content = null;
    /** @var bool */
    private $parsed = false;
    /** @var array */
    private $parsedArray = array();
    /**
     * @var array
     */
    private $nodeConfig = array(
        'node_index' => 'node',
        'value_index' => 'value',
        'children_index' => 'children',
        'attribute_index' => 'attributes'
    );
    /** @var int return tyoe */
    const TYPE_ARRAY = 1;
    /** @var int return tyoe */
    const TYPE_JSON = 2;
    /** Xml Node template */
    const NODE_TEMPLATE = '<%node% %attributes%>%value%%children%</%node%>';
    /** Xml file template %content% will replace by recursively NODE_TEMPLATE */
    const XML_TEMPLATE = '<?xml version="1.0" encoding="utf-8" ?>%content%';

    /**
     * Parser constructor.
     *
     * @param null $file
     * @throws \Exception
     */
    public function __construct($file = null)
    {
        if (!is_null($file)) {
            $this->setFilePath($file);
        }
    }

    /**
     * Set file path to xml and store it
     *
     * @param null $file
     * @throws \Exception
     */
    public function setFilePath($file = null)
    {
        if (!is_null($file) && !file_exists(realpath($file))) {
            throw new \Exception(Helper::ERROR_NOT_VALID_PATH);
        } elseif (!is_null($file) && is_dir(realpath($file))) {
            throw new \Exception(Helper::ERROR_NOT_VALID_PATH_TYPE);
        } else {
            $this->absPath = realpath($file);
        }
    }

    /**
     * Check if parser done or tried to parse already
     *
     * @throws \Exception
     */
    private function checkParsed()
    {
        if ($this->parsed) {
            throw new \Exception(Helper::ERROR_ALREADY_PARSED);
        }
    }

    /**
     * Check if $file passed already
     *
     * @throws \Exception
     */
    private function checkFile()
    {
        if (!is_null($this->absPath)) {
            throw new \Exception(Helper::ERROR_FILE_ALREADY_PASSED);
        }
    }

    /**
     * Parse XML file
     *
     * @param null $file
     * @param int $return
     * @throws \Exception
     * @return boolean
     */
    public function parse($file = null, $return = self::TYPE_ARRAY)
    {
        $this->checkParsed();
        if (!is_null($file)) {
            $this->checkFile();
            $this->setFilePath($file);
        }
        $this->content = file_get_contents($this->absPath);

        return $this->parseString($this->content, $return);
    }

    /**
     * Parse xml content to array
     *
     * @param null $content
     * @param int $returnType
     * @return bool
     * @throws \Exception
     */
    public function parseString($content = null, $returnType = self::TYPE_ARRAY)
    {
        $this->checkParsed();
        if (is_null($content)) {
            throw new \Exception(Helper::ERROR_EMPTY_CONTENT);
        }
        $xml = simplexml_load_string($content);
        $this->parsedArray[] = array(
            $this->nodeConfig['node_index'] => $xml->getName(),
            $this->nodeConfig['value_index'] => trim((String)$xml),
            $this->nodeConfig['attribute_index'] => $this->parseAttributes($xml),
            $this->nodeConfig['children_index'] => $this->parseElement($xml)
        );

        return $this->returnParsed($returnType);
    }

    /**
     * Return parsed data by given returnType or default array
     *
     * @param int $returnType
     * @return array|string
     * @throws \Exception
     */
    private function returnParsed($returnType = self::TYPE_ARRAY)
    {
        if (!in_array($returnType, array(self::TYPE_ARRAY, self::TYPE_JSON))) {
            throw new \Exception(Helper::ERROR_TYPE_INVALID);
        }
        switch ($returnType) {
            case self::TYPE_JSON:
                return json_encode($this->parsedArray);
                break;
            case self::TYPE_ARRAY:
            default:
                return $this->parsedArray;
                break;
        }
    }

    /**
     * Parse SimpleXmlElement to array to process
     *
     * @param \SimpleXMLElement $element
     * @return array
     */
    private function parseElement(\SimpleXMLElement $element)
    {
        $arr = array();
        /** @var \SimpleXMLElement $node */
        foreach ($element as $node) {
            $arr[] = array(
                $this->nodeConfig['node_index'] => $node->getName(),
                $this->nodeConfig['value_index'] => trim((String)$node),
                $this->nodeConfig['attribute_index'] => $this->parseAttributes($node),
                $this->nodeConfig['children_index'] => $this->parseElement($node)
            );
        }

        return $arr;
    }

    /**
     * Parse SimpleXmlElement attributes as array
     *
     * @param \SimpleXMLElement $element
     * @return array
     */
    private function parseAttributes(\SimpleXMLElement $element)
    {
        $tempArray = array();
        foreach ($element->attributes() as $attributeKey => $attributeValue) {
            $tempArray[$attributeKey] = (String)$attributeValue;
        }

        return $tempArray;
    }

    /**
     * Change node parsed keys in array
     *
     * @param string $node
     * @param string $value
     * @param string $children
     * @param string $attribute
     * @return $this
     * @throws \Exception
     */
    public function changeNodeConfig(
        $node = 'node',
        $value = 'value',
        $children = 'children',
        $attribute = 'attributes'
    )
    {
        foreach (func_get_args() as $valuePassed) {
            if (!is_string($valuePassed) || strlen($valuePassed) == 0) {
                throw new \Exception(Helper::PARAM_ERROR);
            }
        }
        $this->nodeConfig = array(
            'node_index' => $node,
            'value_index' => $value,
            'children_index' => $children,
            'attribute_index' => $attribute
        );

        return $this;
    }

    /**
     * Return with content or with boolean value of saved xml file
     *
     * @param array $arrXml
     * @param null $file
     * @return bool|string
     */
    public function toXml($arrXml = array(), $file = null)
    {
        $xml = self::XML_TEMPLATE;
        $content = '';
        if (is_array($arrXml)) {
            $content .= $this->getNodesContent($arrXml);
        }
        $content = str_replace('%content%', $content, $xml);

        if(!$file){
            return $content;
        } else {
            return $this->writeToFile($file, $content);
        }
    }

    /**
     * Save xml content to file and return boolean
     *
     * @param $file
     * @param $content
     *
     * @return boolean
     */
    private function writeToFile($file,$content)
    {
        $fh = fopen($file, 'w+');
        if($fh !== false) {
            $written = fwrite($fh, $content, strlen($content));
            fclose($fh);

            return $written>0;
        }
    }

    /**
     * Return the Node arrays xml content string
     *
     * @param array $arrXml
     * @return null|string
     */
    private function getNodesContent($arrXml = array())
    {
        if (!$arrXml) {
            return null;
        }
        $content = array();
        foreach ($arrXml as $arrNode) {
            $strContent = str_replace(
                array(
                    '%node%',
                    '%attributes%',
                    '%value%',
                    '%children%'
                ),
                array(
                    $this->getArrNodeKeyValue($arrNode, $this->nodeConfig['node_index']),
                    $this->getNodeAttributeContent($this->getArrNodeKeyValue($arrNode, $this->nodeConfig['attribute_index'])),
                    $this->getArrNodeKeyValue($arrNode, $this->nodeConfig['value_index']),
                    $this->getNodesContent($this->getArrNodeKeyValue($arrNode, $this->nodeConfig['children_index']))
                ),
                self::NODE_TEMPLATE
            );
            $content[] = $strContent;
        }

        return join('',$content);
    }

    /**
     * Return the arrNodes value for the given index
     *
     * @param array $arrNode
     * @param null $valueKey
     * @return null
     */
    private function getArrNodeKeyValue($arrNode = [], $valueKey = null)
    {
        if (!is_array($arrNode) || empty($valueKey) || !array_key_exists($valueKey, $arrNode)) {
            return null;
        }

        return $arrNode[$valueKey];
    }

    /**
     * Returns the attribute concatenated string
     *
     * @param array $attributes
     * @return string
     */
    private function getNodeAttributeContent($attributes = [])
    {
        $arrAttributes = [];
        if(is_array($attributes)){
            foreach($attributes as $key=>$value){
                $arrAttributes[] = "{$key}=\"{$value}\"";
            }
        }

        return join(' ', $arrAttributes);
    }
}