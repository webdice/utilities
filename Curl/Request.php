<?php
/**
 * Copyright © 2016 Dombi István All rights reserved.
 * See COPYING.txt for license details.
 */
/**
 * PHP version 5.6
 *
 * @category  Webdice
 * @author    Dombi István <dombi.istvan@webdice.hu> <dombiistvan28@gmail.com>
 * @copyright 2015 Dombi István
 */
namespace Webdice\Utilities\Curl;

class Request
{
    /**
     * @var null|resource
     */
    private $ch = null;

    /** @var  array */
    private $info;
    /**
     * @var array
     */
    private $options = array();
    /**
     * @var array
     */
    private $responseHeaders = array();
    /**
     * @var null|mixed
     */
    private $response = null;

    /**
     * Initialize curl request, set given options
     *
     * @param null $url
     * @param array $options
     * @throws \Exception
     */
    public function __construct($url = null, $options = array())
    {
        if (!is_string($url) || !filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \Exception(Helper::ERROR_NO_URL);
        }
        if (is_array($options) && count($options)) {
            $this->setOptions($options);
        }
        $this->ch = curl_init();
        $this->setOption('url', $url);
        $this->setOption('headerfunction', array($this, 'parseHeader'));
    }

    /**
     * Truncate options and set given options array to curl
     *
     * @param array $options
     * @return $this
     * @throws \Exception
     */
    public function setOptions($options = array())
    {
        if (!is_array($options)) {
            throw new \Exception(Helper::ERROR_CURL_OPTIONS_TYPE);
        }
        $this->options = null;
        $this->addOptions($options);

        return $this;
    }

    /**
     * Return curl options except the keys passed in except
     * - can pass as other options with or without curlopt_
     * and lowercase
     *
     * @param array $except the array of skippable options
     *
     * @return array options of curl
     */
    public function getOptions($except = array())
    {
        if (is_array($except) && count($except)) {
            $returnOptions = $this->options;
            foreach ($except as $key) {
                if (array_key_exists($this->getConstKey($key), $returnOptions)) {
                    unset($returnOptions[$this->getConstKey($key)]);
                }
            }

            return $returnOptions;
        }

        return $this->options;
    }

    /**
     * Add options to curl, redefine will overwrite existing ones
     *
     * @param array $options
     * @return $this
     * @throws \Exception
     */
    public function addOptions($options = array())
    {
        if (!is_array($options)) {
            throw new \Exception(Helper::ERROR_CURL_OPTIONS_TYPE);
        }
        foreach ($options as $key => $value) {
            $this->setOption($key, $value);
        }

        return $this;
    }

    /**
     * Return the option to the given key, or default passed
     *
     * @param null $key
     * @param null $default
     * @return null
     * @throws \Exception
     */
    public function getOption($key = null, $default = null)
    {
        if (is_null($key)) {
            throw new \Exception(sprintf(Helper::ERROR_CURL_OPTION_MISSING, 'key'));
        }
        if (array_key_exists($key, $this->getOptions())) {
            return $this->getOptions()[$key];
        } elseif (array_key_exists($this->getConstKey($key), $this->getOptions())) {
            return $this->getOptions()[$this->getConstKey($key)];
        } else {
            return $default;
        }
    }

    /**
     * Set single option value to curl
     *
     * @param null $key
     * @param null $value
     * @return $this
     * @throws \Exception
     */
    public function setOption($key = null, $value = null)
    {
        if (is_null($key) || is_null($value)) {
            throw new \Exception(Helper::ERROR_CURL_EMPTY_PARAM_OR_VALUE);
        }
        $this->options[$this->getConstKey($key)] = $value;

        return $this;
    }

    /**
     * Request the given url with GET method
     *
     * @param array $data
     * @return $this
     * @throws \Exception
     */
    public function get($data = array())
    {
        $params = is_array($data) && !empty($data) ? http_build_query($data) : false;
        $newUrl = false === $params ? $this->getOption('url') : ($this->getOption('url') . '?' . $params);
        $this->setOption('url', $newUrl);

        return $this->send();
    }

    /**
     * Post the given data array with CURL
     *
     * @param array $data
     * @return $this
     * @throws \Exception
     */
    public function post($data = array())
    {
        if ((!is_array($data) || empty($data)) && !$this->hasOption('postfields')) {
            throw new \Exception(sprintf(Helper::ERROR_CURL_OPTION_MISSING, 'CURLOPT_POSTFIELDS'));
        }
        $this->setOption('post', true);
        if (is_array($data)) {
            $this->setOption('postfields', $data);
        }

        return $this->send();
    }

    /**
     * Return option already set
     *
     * @param null $option
     * @return bool
     * @throws \Exception
     */
    public function hasOption($option = null)
    {
        if (is_null($option)) {
            throw new \Exception(Helper::ERROR_CURL_OPTION_EMPTY);
        }
        $constKey = is_string($option) ? $this->getConstKey($option) : $option;

        return array_key_exists($constKey, $this->getOptions());
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function send()
    {
        $this->setCurlOptions();
        if (false === ($this->response = curl_exec($this->ch)) || 0 !== curl_errno($this->ch)) {
            throw new \Exception(sprintf(Helper::ERROR_CURL_REQUEST_FAILED, curl_error($this->ch)));
        }

        $this->info = curl_getinfo($this->ch);

        curl_close($this->ch);

        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Parse header row and set to $responseHeaders
     *
     * @param null $resource
     * @param null $headerRow
     * @return int
     */
    private function parseHeader($resource = null, $headerRow = null)
    {
        if (preg_match('%^HTTP\/\d{1}+\.+\d{1}+\s+(\d+)\s?[A-z]*\s?$%', $headerRow, $match)) {
            $this->responseHeaders['Status-Code'] = $match[1];
        } elseif (preg_match('%^([A-z0-9]*)\s?\:+\s?(.*)$%', $headerRow, $match)) {
            $this->responseHeaders[$match[1]] = trim($match[2]);
        }

        return strlen($headerRow);
    }

    /**
     * @return mixed
     */
    public function getResponseHeaders()
    {
        return $this->responseHeaders;
    }

    /**
     * Return the content of response header option, or null
     *
     * @param null $header
     * @return null
     * @throws \Exception
     */
    public function getResponseHeader($header = null)
    {
        if (is_null($header) || empty($header)) {
            throw new \Exception(sprintf(Helper::ERROR_CURL_EMPTY_PARAM_OR_VALUE, 'header'));
        }
        if (array_key_exists($header, $this->responseHeaders)) {
            return $this->responseHeaders[$header];
        }

        return null;
    }

    /**Set
     *
     * @return $this
     * @throws \Exception
     */
    private function setCurlOptions()
    {
        foreach ($this->getOptions() as $key => $value) {
            curl_setopt($this->ch, $this->getConstValue($key), $value);
        }

        return $this;
    }

    /**
     * Return the real const value of the key
     *
     * @param null|string $key
     * @return integer|string
     * @throws \Exception
     */
    private function getConstValue($key = null)
    {
        if (is_null($key)) {
            throw new \Exception(Helper::ERROR_CURL_EMPTY_PARAM_OR_VALUE);
        }
        $key = strtolower($key);
        if (is_string($key)) {
            $constKey = $this->getConstKey($key);

            return constant(strtoupper($constKey));
        } elseif (is_int($key)) {
            return $key;
        }
    }

    /**
     * Return constant key for param
     *
     * @param string $key
     * @return mixed|null|string
     * @throws \Exception
     */
    private function getConstKey($key = null)
    {
        if (is_null($key)) {
            throw new \Exception(Helper::ERROR_CURL_EMPTY_PARAM_OR_VALUE);
        }
        $key = strtolower($key);
        $constKey = strpos($key, 'curlopt_') === 0 || strpos($key, 'curlinfo_') === 0 ? $key : "curlopt_{$key}";
        if (!constant(strtoupper($constKey))) {
            $constKey2 = str_replace('curlopt_', 'curlinfo_', $constKey);
            if (!constant(strtoupper($constKey2))) {
                throw new \Exception(sprintf(Helper::ERROR_CURL_INVALID_OPTION, $key));
            }
            $constKey = $constKey2;
        }

        return $constKey;
    }

    /**
     * Print request and response data
     *
     * @param $options boolean debug CURL options
     * @param $headers boolean debug response headers
     * @param $response boolean debug curl response
     *
     * @return $this
     */
    public function debug($options = true, $headers = true, $response = true)
    {
        echo '<pre>';
        if ($options) {
            print_r($this->getOptions());
        }
        if ($headers) {
            print_r($this->getResponseHeaders());
        }
        if ($response) {
            print_r(htmlspecialchars($this->getResponse()));
        }
        echo '</pre><hr />';
    }

    /**
     * @return array curl info
     */
    public function getInfo()
    {
        return $this->info;
    }
}