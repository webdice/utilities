# README #

This Repository contains useful classes to make some of your work much easier. Currently cURL Request and Xml Parser/Writer classes, with example codes.

* CURL Request
* XML file/string Parser/Writer

### What is this repository for? ###

* This repository for to have an easy group of code to the usually not easy progresses
* 0.2.0

### How do I get set up? ###

* Init, install if you don't have it already or update composer
* copy curl_examples.php, curl_file_receive.php, xml_parser_example.php, test.jpg and test.xml files to your web root, and run
* Open the example file you want to see (curl,xml)
* use and enjoy

## CURL Examples ##

* Example GET Request

```
#!php
$request = new Webdice\Utilities\Curl\Request('http://example.com' , array('returntransfer' => 1));
$request->get(array('something' => 'value', 'other' => 'value2'));
```

* Example POST request

```
#!php
$request = new Webdice\Utilities\Curl\Request('http://posttestserver.com/post.php?dir=webdice' , array('returntransfer' => 1));
$request->post(array('something' => 'value', 'other' => 'value2'));
```
* Example POST request with file upload

```
#!php
$file = realpath('test.jpg');
$request = new Webdice\Utilities\Curl\Request('http://things.local/curl_file_receive.php' , array('safe_upload' => false));
$request->post(array('file' => '@' . $file,'post' => 'value'));
```
* Example Custom request

```
#!php
$request = new Webdice\Utilities\Curl\Request('http://example.com' , array('returntransfer' => 1,'customrequest' => 'PUT'));
$request->send();

```

## XML Parser examples ##


* Example: Pass xml file to parse

```
#!php
$parser = new \Webdice\Utilities\Xml\Parser('test.xml');
$arr = $parser->parse();
var_dump($arr);

```

* Example: Parse xml content directly
```
#!php
$parser = new \Webdice\Utilities\Xml\Parser();
$arr = $parser->parseString('<root_element><items><item attributex="1">dsa</item></items></root_element>');
var_dump($arr);
```

* Example: Change the return data keys !before parse method
```
#!php
$parser = new \Webdice\Utilities\Xml\Parser('test.xml');
$parser->changeNodeConfig('element_name', 'children_elements', 'attributes');
$arr = $parser->parse();
var_dump($arr);
```

* Example: Change parsed response format to JSON 
```
#!php
parser = new \Webdice\Utilities\Xml\Parser();
$parser->changeNodeConfig('element_name', 'children_elements', 'attributes');
$arr = $parser->parse('test.xml', \Webdice\Utilities\Xml\Parser::TYPE_JSON);
```

* Example: Write xml file from array (recursively)
```
#!php
$parser = new \Webdice\Utilities\Xml\Parser();
$content = $parser->toXml(array(
    array(
        'node' => 'valami',
        'value' => '',
        'children' => array(
            array(
                'node' => 'valami1',
                'value' => 'dsa',
                'children' => array(
                    array(
                        'node' => 'valami2',
                        'value' => 'dsa1',
                        'children' => array(
                            array(
                                'node' => 'valami3',
                                'value' => 'dsa2',
                                'attributes' => array(
                                    'dd3' => 7,
                                    'dd4' => 8
                                )
                            ), array(
                                'node' => 'valami4',
                                'value' => 'dsa3',
                                'attributes' => array(
                                    'dd3' => 9,
                                    'dd4' => 10
                                )
                            ), array(
                                'node' => 'valami5',
                                'value' => 'dsa4',
                                'attributes' => array(
                                    'dd3' => 11,
                                    'dd4' => 12
                                )
                            ),
                        ), 
                        'attributes' => array(
                            'dd3' => 5,
                            'dd4' => 6
                        )
                    )
                ), 
                'attributes' => array(
                    'dd3' => 3,
                    'dd4' => 4
                )
            )
        ), 
        'attributes' => array(
            'dd1' => 1,
            'dd2' => 2
        )
    )
), 'temp.xml');


```
### Who do I talk to? ###

* Repo owner or admin
* Write an email to <dombi.istvan@webdice.hu>