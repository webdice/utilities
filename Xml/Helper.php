<?php
/**
 * Copyright © 2016 Dombi István All rights reserved.
 * See COPYING.txt for license details.
 */
/**
 * PHP version 5.6
 *
 * @category  Aion
 * @author    Dombi István <dombi.istvan@webdice.hu>
 * @copyright 2015 AionNext Kft. (http://www.aion.hu)
 * @link      http://www.webdice.hu
 */
namespace Webdice\Utilities\Xml;

class Helper
{
    const ERROR_NOT_VALID_PATH = 'The passed path is not valid.';
    const ERROR_NOT_VALID_PATH_TYPE = 'The passed path is a file.';
    const ERROR_ALREADY_PARSED = 'The Xml already parsed or tried to parse.';
    const ERROR_FILE_ALREADY_PASSED = 'The Xml file already passed.';
    const ERROR_EMPTY_CONTENT = 'The Xml content is empty.';
    const ERROR_TYPE_INVALID = 'The Xml return type is invalid.';
    const PARAM_ERROR = 'The parameter passed is not valid.';
}