<?php
/**
 * Creaated by Dombi István <dombi.istvan@webdice.hu> <dombiistvan28@gmail.com>
 */
require_once dirname(__FILE__) . '/vendor/autoload.php';

/**
 * Example GET request
 */
$request = new Webdice\Utilities\Curl\Request('http://example.com', array('returntransfer' => 1));
$request->get(array('something' => 'value', 'other' => 'value2'));
//$request->debug();

/**
 * Example POST request
 */
$request = new Webdice\Utilities\Curl\Request(
    'http://posttestserver.com/post.php?dir=webdice',
    array('returntransfer' => 1)
);
$request->post(array('something' => 'value', 'other' => 'value2'));
//$request->debug();

/**
 * Exmaple POST request WITH FILE TRANSFER
 */
$file = realpath('test.jpg');
$url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$url = substr($url,0,strpos($url,'examples.php'));
$request = new Webdice\Utilities\Curl\Request(
    $url . 'file_receive.php',
    array(
        'safe_upload' => false
    )
);
$request->post(array(
    'file' => '@' . $file,
    'post' => 'value'
));

/**
 * Example CUSTOM REQUEST
 */
$request = new Webdice\Utilities\Curl\Request('http://example.com', array(
    'returntransfer' => 1,
    'customrequest' => 'PUT'
));
$request->send();
// and get response of request
$request->getResponse();
// or get specified header
$request->getResponseHeader('Content-type');
// or get all headers
$request->getResponseHeaders();
