<?php
/**
 * Creaated by Dombi István <dombi.istvan@webdice.hu> <dombiistvan28@gmail.com>
 */
//only if you dont have composer
//require_once dirname(__FILE__) . '/vendor/autoload.php';

//pass the xml file like this
$parser = new \Webdice\Utilities\Xml\Parser('test.xml'); //absolute URL or realive to dirname(__FILE__)
$arr = $parser->parse();
//var_dump($arr);

//or parse the string directly like this
$parser = new \Webdice\Utilities\Xml\Parser();
$arr = $parser->parseString('<root_element><items><item attributex="1">dsa</item></items></root_element>');
//var_dump($arr);

//you can also change the return array keys like this, !before the parse method
$parser = new \Webdice\Utilities\Xml\Parser('test.xml');
$parser->changeNodeConfig('element_name', 'children_elements', 'attributes');
$arr = $parser->parse(null);
//var_dump($arr);

//you can change format between JSON and ARRAY like this
$parser = new \Webdice\Utilities\Xml\Parser();
$parser->changeNodeConfig('element_name', 'children_elements', 'attributes');
$arr = $parser->parse('test.xml', \Webdice\Utilities\Xml\Parser::TYPE_JSON);

//and you can write xml from array recursively
$parser = new \Webdice\Utilities\Xml\Parser();
$content = $parser->toXml(
    array(
        array(
            'node' => 'valami',
            'value' => '',
            'children' => array(
                array(
                    'node' => 'valami1',
                    'value' => 'dsa',
                    'children' => array(
                        array(
                            'node' => 'valami2',
                            'value' => 'dsa1',
                            'children' => array(
                                array(
                                    'node' => 'valami3',
                                    'value' => 'dsa2',
                                    'attributes' => array(
                                        'dd3' => 7,
                                        'dd4' => 8
                                    )
                                ), array(
                                    'node' => 'valami4',
                                    'value' => 'dsa3',
                                    'attributes' => array(
                                        'dd3' => 9,
                                        'dd4' => 10
                                    )
                                ), array(
                                    'node' => 'valami5',
                                    'value' => 'dsa4',
                                    'attributes' => array(
                                        'dd3' => 11,
                                        'dd4' => 12
                                    )
                                ),
                            ), 'attributes' => array(
                            'dd3' => 5,
                            'dd4' => 6
                        )
                        )
                    ), 'attributes' => array(
                    'dd3' => 3,
                    'dd4' => 4
                )
                )
            ), 'attributes' => array(
            'dd1' => 1,
            'dd2' => 2
        )
        )
    ), 'temp.xml'
);